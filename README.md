### Old TODO

- Предложение сделать 2 кнопки вместо одной:
  - все АЗС «Газпром» (не заправки,а АЗС)
  - ближайшие АЗС «Газпром»

- Предложение – сразу запрашивать местоположение пользователя, чтобы не догадываться нажимать кнопку «стрелка» геолокации. Тогда предложение «Ближайшие АЗС» будет логичным –  сразу нажму и увижу, что есть рядом со мной.
 
- Убрать слово Маршрут и добавить кнопку печать.

- Заменить цвет маршрута с фиолетового на темно-синий. Убрать зажигалочку. Заменим потом на лого, которое дадут.

- Добавить, если можно, расстояние до заправки как у роснефти:-) http://www.rosneft-azs.ru/about
